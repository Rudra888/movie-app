package com.example.moviefinder.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.moviefinder.R
import com.example.moviefinder.adapter.Adapter

lateinit var adapterV: Adapter
lateinit var moviesRecyclerView: RecyclerView
class MainFragment : Fragment() {

    private lateinit var viewModel: MainViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {

        val view = inflater.inflate(R.layout.main_fragment, container, false)
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        viewModel.getMovie()

        moviesRecyclerView = view.findViewById(R.id.recycler_view_container)
        moviesRecyclerView.layoutManager = GridLayoutManager(requireContext(), 2)
        adapterV = Adapter()


        moviesRecyclerView.adapter = adapterV

        viewModel.result.observe(viewLifecycleOwner) {
           // rv.adapter = adapterV
            adapterV.setMovieData(it)

        }
        return view
    }
}