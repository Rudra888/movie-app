package com.example.moviefinder.ui.main

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.moviefinder.dataclass.FullResponse
import com.example.moviefinder.dataclass.SingleResponse
import com.example.moviefinder.networking.MoviesRetrofit
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainViewModel : ViewModel() {

    private val _result = MutableLiveData<List<SingleResponse>>()
    var result: LiveData<List<SingleResponse>> = _result

    fun getMovie() {
        val movies = MoviesRetrofit.moviesFetch.getMovies()
        movies.enqueue(object : Callback<FullResponse> {
            override fun onFailure(call: Call<FullResponse>, t: Throwable) {
                Log.d("Rudra", "Error")
            }

            override fun onResponse(call: Call<FullResponse>, response: Response<FullResponse>) {
                val moviesinfo = response.body()
                if (moviesinfo != null) {
                    _result.postValue(moviesinfo.results)
                }
            }
        })
    }

}