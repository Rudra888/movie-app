package com.example.moviefinder.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.moviefinder.R
import com.example.moviefinder.dataclass.SingleResponse
import com.example.moviefinder.viewholder.ViewHolder

class Adapter : RecyclerView.Adapter<ViewHolder>() {
    var movieList = mutableListOf<SingleResponse>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.recycler_view_movie_item, parent, false)

        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return movieList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.setImage(movieList[position])

    }

    fun setMovieData(lists: List<SingleResponse>) {
        movieList.addAll(lists)
    }

}