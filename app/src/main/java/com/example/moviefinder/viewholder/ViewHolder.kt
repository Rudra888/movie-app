package com.example.moviefinder.viewholder

import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.moviefinder.R
import com.example.moviefinder.dataclass.SingleResponse

class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val recyclerItem = itemView.findViewById<ImageView>(R.id.recycler_view_image)

    fun setImage(singleResponse: SingleResponse) {
        val posterPath = "https://image.tmdb.org/t/p/w185${singleResponse.poster_path}"
        Glide.with(itemView.context).load(posterPath).into(recyclerItem)


    }
}