package com.example.moviefinder.networking

import com.example.moviefinder.dataclass.FullResponse
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET


//https://api.themoviedb.org/3/movie/550?api_key=65db5aebb7dc29d77c7b00443904e829
const val BASE_URL = "https://api.themoviedb.org"
const val KEY = "65db5aebb7dc29d77c7b00443904e829"

interface MoviesFetch {
    @GET("/3/movie/popular?api_key=${KEY}")
    fun getMovies(): Call<FullResponse>
}

object MoviesRetrofit {
    val moviesFetch: MoviesFetch

    init {
        val retrofit =
            Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create())
                .build()
        moviesFetch = retrofit.create(MoviesFetch::class.java)
    }
}