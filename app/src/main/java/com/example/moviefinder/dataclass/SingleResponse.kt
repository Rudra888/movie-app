package com.example.moviefinder.dataclass

data class SingleResponse(val poster_path: String)