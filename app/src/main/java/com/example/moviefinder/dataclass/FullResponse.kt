package com.example.moviefinder.dataclass

data class FullResponse(val page: Int, val results: List<SingleResponse>)