package com.example.moviefinder.interfaces

import com.example.moviefinder.dataclass.SingleResponse

interface MovieClickListner {
    fun onMovieClick(singleResponse: SingleResponse)
}